from django.test import TestCase, Client
from django.urls import resolve
from .views import index

class AppHomeUnitTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/home/')
		self.assertEqual(response.status_code, 200)

	def test_is_using_index_func(self):
		found = resolve('/home/')
		self.assertEqual(found.func, index)

	def test_template_using_home_html(self):
		response = Client().get('/home/')
		self.assertTemplateUsed(response, "app_home/app_home.html")
