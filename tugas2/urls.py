"""tugas2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import app_home.urls as app_home
import app_profile.urls as app_profile
import app_forum.urls as app_forum

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^home/', include(app_home,namespace='app_home')),
    url(r'^company/forum/', include(app_forum,namespace='app_forum')),
    url(r'^company/', include(app_profile,namespace='app_profile')),
    url(r'^$',RedirectView.as_view(url='home/',permanent=True),name='index'),
]
