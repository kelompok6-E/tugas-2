from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

response = {}
def index(request):
	html = 'app_profile/app_profile.html'
	return render(request, html, response)

def unfinished(request):
	html = 'app_profile/almost-there.html'
	return render(request, html, response)