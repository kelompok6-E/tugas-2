from django.conf.urls import url
from .views import index, unfinished

urlpatterns = [
    url(r'^profile/$', index, name='index'),
    url(r'^under-construction/$', unfinished, name='under-construction'),
]