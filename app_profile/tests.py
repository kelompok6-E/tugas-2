from django.http import HttpRequest
from django.test import Client, TestCase
from django.urls import resolve
from .views import index, unfinished

# Create your tests here.
class ProfileUnitTest(TestCase):
    def test_profile_url_is_exist(self):
        response = Client().get('/company/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_using_index_func(self):
        found = resolve('/company/profile/')
        self.assertEqual(found.func, index)

    def test_underconst_url_is_exist(self):
        response = Client().get('/company/under-construction/')
        self.assertEqual(response.status_code, 200)

    def test_underconst_using_index_unfinished(self):
        found = resolve('/company/under-construction/')
        self.assertEqual(found.func, unfinished)