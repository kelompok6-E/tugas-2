function logout() {
  IN.User.logout();
}

function onLinkedInLoad() {
  IN.Event.on(IN, "auth", getCorporationData);
}

function getCorporationData() {
  IN.API.Raw("companies?format=json&is-company-admin=true")
  	.method("GET")
  	.result(function(result){
      var id = result.values[0].id;
      getData(result.values[0].id);
  	})
  	.error(function(result){
  		alert("error");
  	});
}

function getData(result) {
  IN.API.Raw("companies/"+result+":(logo-url,name,company-type,website-url,specialties)?format=json")
    .method("GET")
    .result(function(result){
      printData(result)
    })
    .error(function(result){ 
      alert("error")
    });
}

function printData(result) {
  document.getElementById("welcome-message").innerHTML = "Selamat Datang, "+ result.name;
  document.getElementById("corp-small-image").src= result.logoUrl;
  document.getElementById("tipe").innerHTML = result.companyType.name;
  document.getElementById("web").innerHTML = result.websiteUrl;
  document.getElementById("spesialitas").innerHTML = result.specialties.values;
}

function underconst() {
    window.location.replace("/company/under-construction");
}

function toForum() {
  window.location.replace("/company/forum");
}