from django.db import models

# Create your models here.
class Forum(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    content = models.TextField(max_length=500)
    company_id = models.IntegerField(default=0)