from django import forms

class ForumForm(forms.Form):
	content_attrs = {
		'class': 'form-control',
		'placeholder': 'Insert Description'
	}
	content = forms.CharField(max_length=500, widget=forms.TextInput(attrs=content_attrs))